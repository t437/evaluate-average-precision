# get txt file for each xml file containing categories of detected objects and its coordinates. 
# Following this repo https://github.com/Cartucho/mAP#create-the-ground-truth-files based on PASCAL VOC 2012 competition


import glob
import xml.etree.ElementTree as ET

# Pascal VOC Bounding box is defined by (x-top left (min), y-top left (min), x-bottom right (max), y-bottom right (max))
# desired format: <class_name> <left> <top> <right> <bottom>   (xmin, ymin, xmax, ymax)

def xml_to_txt():
    folders = ['Torx.v26-v7.voc/test', 'Torx.v26-v7.voc/train', 'Torx.v26-v7.voc/valid']
    #path = 'Torx.v26-v7.voc/valid'
    # iterate through all xml files
    for folder in folders:
        for file in glob.glob(folder + '/*.xml'):
            # open xml file
            root = ET.parse(file).getroot()
            #get all objects in file
            image = root.find('filename').text
            img_name = image[:-len('.jpg')]
            xml_file_name = "{}/{}.txt".format(folder, img_name)
            with open(xml_file_name, "w") as file_temp:
                last_element = root.findall('object')[-1]
                for member in root.findall('object'):
                    x_min = int(member[5][0].text)
                    y_min = int(member[5][2].text)
                    x_max = int(member[5][1].text)
                    y_max = int(member[5][3].text)
                    object_class = member[0].text
                    # don't add newline if last record
                    if member == last_element:
                        str_data = "{} {} {} {} {}".format(object_class, x_min, y_min, x_max, y_max)
                    else:
                        str_data = "{} {} {} {} {}\n".format(object_class, x_min, y_min, x_max, y_max)
                    file_temp.write(str_data)
            
            file_temp.close()


xml_to_txt()