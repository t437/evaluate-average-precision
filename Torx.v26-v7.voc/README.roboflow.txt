
Torx - v26 v7
==============================

This dataset was exported via roboflow.ai on September 23, 2021 at 9:06 AM GMT

It includes 1730 images.
Torx are annotated in Pascal VOC format.

The following pre-processing was applied to each image:

No image augmentation techniques were applied.


